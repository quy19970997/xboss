/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import Axios from 'axios';
import {Platform, StyleSheet, Text, View,
        ImageBackground,
        Image,Dimensions,TouchableOpacity,Icon,
      TextInput} from 'react-native';
import {
        GoogleSignin,
        GoogleSigninButton,
        statusCodes,
      } from 'react-native-google-signin';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


import bgImage from './images/a.png'
import logo from './images/b.png'
import logoGG from './images/g4.png'
import axios from 'axios';
const {width: WIDTH} = Dimensions.get('window')
export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username:'',
      password:'',
      userInfo: '',
    }
  }
  render() {
    return (
     
      <ImageBackground source={bgImage} style={styles.backgroundContainer}>
        <View style={styles.Container}>
          <View style={styles.logoContainer}>
            <Image source={logo} style={styles.logo}/>
          </View>
          <View style={styles.inputContainer}>
            <TextInput  style={[styles.TextInput, styles.borderBottom]}
                        placeholder={'Username'}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        underlineColorAndroid='transparent'
                        onChangeText={(text)=>{
                          this.setState({username:text});
                        }}>
            </TextInput>
            <TextInput  style={[styles.TextInput, styles.borderBottom]}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        placeholderTextColor={'rgba(255,255,255,0.7)'}
                        underlineColorAndroid='transparent'
                        onChangeText={(text)=>{
                          this.setState({password:text});
                        }}>
            </TextInput>
            <View style={styles.buttonLogin}>
              <TouchableOpacity style={styles.btLogin}
              onPress={()=>{
                axios.post('https://test01.e300.vn/web/session/authenticate',{
                  params:{
                    db:'hhd_e300_vn_test01',
                    login:this.state.username,
                    password:this.state.password
                  }
                }).then(response => {
                    alert(response.data.result.username)
                    console.log(response.data.result.username)
                    return response
                })
              }}>
                  <Text style={styles.text}>Login</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.container}>
              <TouchableOpacity style={styles.btLoginGG}
              onPress={this._signIn}>
                <Image source={logoGG} style={{width:25, height:25}}/>
                <Text style={styles.textGG}>LOGIN WITH GOOGLE</Text>
              </TouchableOpacity>
              
            </View>
          </View>
        </View>
      </ImageBackground>
    
    );
  }
  componentDidMount() {
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId:
        '606209676315-ojnas0b4jucuisp5vni29hd63dgtmh17.apps.googleusercontent.com',
    });
  }
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log('aaaaa', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
  _getCurrentUser = async () => {
    //May be called eg. in the componentDidMount of your main component.
    //This method returns the current user
    //if they already signed in and null otherwise.
    try {
      const userInfo = await GoogleSignin.signInSilently();
      this.setState({ userInfo });
    } catch (error) {
      console.error(error);
    }
  };
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };
  _revokeAccess = async () => {
    //Remove your application from the user authorized applications.
    try {
      await GoogleSignin.revokeAccess();
      console.log('deleted');
    } catch (error) {
      console.error(error);
    }
  };
}
const styles = StyleSheet.create({
  
  backgroundContainer: {
    borderRadius:45,
   // backgroundColor='rgba(28,117,188,0.8)',
    flex: 1,
    width:null,
    height:null,
  },
  Container:{
    flex  :1,
    backgroundColor:'rgba(28,117,188,0.9)',
  },
  logoContainer:{
    flex:1,
    paddingTop: 100,
    alignItems:'center',
  },
  logo:{
    width:200,
    height:60
  },
  inputContainer:{
    flex:2,
    padding: 20,
  },
  borderBottom:{
    borderBottomColor:'white',
    borderBottomWidth:1
  },
  textInput:{
    borderBottomWidth: 5,
    width:WIDTH,
    height:45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor:'rgba(0,0,0,0.35)',
    color :'rgba(255,255,255,0.7)',
    marginHorizontal: '25',
  },
  buttonLogin:{
    marginTop:10,
    borderRadius: 5,
    height:45,
    backgroundColor:'#65c8cd',
    justifyContent:'center',
    fontWeight:'bold',
  },
  
  text:{
    color:'white',
    fontSize:16,
    textAlign:'center',
    fontWeight:'bold'
  },
  container: {
    
    marginTop: 10,
    borderRadius:5,
    height:45,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight:'bold',
  
  },
  btLoginGG:{

    flexDirection: 'row',
  },
  textGG:{
    color:'blue',
    fontSize:16,
    textAlign:'center',
    fontWeight:'bold'
  },
});
